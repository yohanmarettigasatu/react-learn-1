// import logo from './logo.svg';
// import './App.css';
// import ExpenseItem from './components/expenseItems/ExpenseItem';
import {useState} from 'react';
import Expense from './components/Expense/Expense';
import NewExpense from './components/NewExpense/NewExpense';
const DUMMY_EXPENSES = [
  {id: '1', date: new Date(2022, 1, 1), title: 'Car 1', price: '3000'},
  {id: '2', date: new Date(2022, 1, 2), title: 'Car 2', price: '3100'},
  {id: '3', date: new Date(2022, 1, 3), title: 'Car 3', price: '3200'}
]
function App() {
 
  const [expenses, setResult] = useState(DUMMY_EXPENSES);

  const newDataHandler = (elm)=>{
    setResult(prevState =>{
      return [elm, ...prevState]});
  }
  return (
    <div className="App">
      <header className="App-header">
        <NewExpense onNewData={newDataHandler}/>
        <Expense items={expenses}/>
      </header>
    </div>
  );
}

export default App;
