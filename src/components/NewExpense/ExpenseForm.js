import React, { useState } from 'react';
import './ExpenseForm.css';

const ExpenseForm = (props) => {
  const [input, setInput] = useState({
    date: '',
    price: '',
    title: '',
  })
  const titleHandler = (event)=>{
    setInput((prevState)=>{
      return {...prevState, title: event.target.value}
    })
  }
  const priceHandler = (event)=>{
    setInput((prevState)=>{
      return {...prevState, price: event.target.value}
    })
  }
  const dateHandler = (event)=>{
    setInput((prevState)=>{
      return {...prevState, date: event.target.value}
    })
  }
  const submitHandler = (event)=>{
    event.preventDefault();
   
    setInput((prevState)=>{
      return { date: '', price: '', title: ''}
    })
    const data = {
      title: input.title,
      price: +input.price,
      date: new Date(input.date)
    }
    props.onSaveExpenseData(data);
  }
  return (
    <form onSubmit={submitHandler}>
        <div className='new-expense__controls'>
          <div className='new-expense__control'>
            <label>Title</label>
            <input type="text" value={input.title} onChange={titleHandler}/>
          </div>
          <div className='new-expense__control'>
            <label>Price</label>
            <input type="number" value={input.price} min="0.01" step="0.01"onChange={priceHandler} />
          </div>
          <div className='new-expense__control'>
            <label>Date</label>
            <input type="date" value={input.date} min="2020-02-01" max="2023-12-30" onChange={dateHandler}/>
          </div>
        </div>
        <div className="new-expense__actions">
          <button type='button' onClick={props.onCancelSubmit}>cancel</button>
          <button type='submit' >Submit</button>
        </div>
    </form>
  )
}

export default ExpenseForm