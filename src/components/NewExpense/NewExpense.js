import { useState } from 'react';
import './NewExpense.css';
import ExpenseForm from './ExpenseForm';
const NewExpense = (props) => {
  const saveDataHandler = (data)=>{
    const item = {
      ...data,
      id: Math.random().toString(),
    }
    props.onNewData(item);
  }
  const [form, setForm] = useState(false);
  const onOpenHandler = ()=>{
    setForm(true)
  }
  const cancelSubmitHandler = ()=>{
    setForm(false);
  }
  return (
    <div className='new-expense'>
        
        {form ? 
          <div>
            <ExpenseForm onSaveExpenseData={saveDataHandler} onCancelSubmit={cancelSubmitHandler}/>
          </div>
          : <button onClick={onOpenHandler}>Add New Expense</button>
        }

    </div>
  )
}

export default NewExpense