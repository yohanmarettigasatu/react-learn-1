import React from 'react'
import './Chart.css'
import ChartBar from './ChartBar'

const Chart = (props) => {
  console.log(props.items)
  const data = props.items.map((elm)=> elm.value)
  console.log(data);
  const totalMax = Math.max(...data);
  return (
    <div className='chart'>
        {props.items.map((elm)=>{
            return <ChartBar 
                key={elm.label}
                value={elm.value}
                maxValue={totalMax}
                label={elm.label}
            />
        })
        }
    </div>
  )
}

export default Chart