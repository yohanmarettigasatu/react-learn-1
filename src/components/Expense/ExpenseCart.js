import React from 'react'
import './ExpenseCart.css'
import Chart from '../Chart/Chart'
const ExpenseCart = (props) => {
  console.log('props.expenses')
  console.log(props.expenses)
  const data = [
    {label:'Jan', value: 0},
    {label:'Feb', value: 0},
    {label:'Mar', value: 0},
    {label:'Apr', value: 0},
    {label:'May', value: 0},
    {label:'Jun', value: 0},
    {label:'Jul', value: 0},
    {label:'Aug', value: 0},
    {label:'Sep', value: 0},
    {label:'Oct', value: 0},
    {label:'Nov', value: 0},
    {label:'Dev', value: 0},
  ];
  for (const expense of props.expenses){
    const expenseMonth = expense.date.getMonth();
    data[expenseMonth].value += expense.price;
  }
  return (
    <Chart items={data} />
  )
}

export default ExpenseCart