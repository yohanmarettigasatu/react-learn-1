import React, { useState } from "react";

import './ExpenseItem.css';
import ExpenseDate from "./ExpenseDate";
import Card from "../ui/Card";
const ExpenseItem = ({props}) => {
  // console.log(props.title)
  const [title, setTitle] = useState(props.title);
  const clickHandler= ()=> {
    setTitle('oke');
  };
  return (
    <Card className= "expense-item">
      <div className="expense-item__description">
        <ExpenseDate date={props.date}/>
        <h2>{title}</h2>
        <div className="expense-item__price">{props.price}</div>
        <button onClick={clickHandler}>Change Title</button>
      </div>
    </Card>
  );
};

export default ExpenseItem;
