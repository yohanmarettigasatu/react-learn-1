import ExpenseItem from "./ExpenseItem"
import './ExpenseList.css';

const ExpenseList = (props) => {
  if (props.items.length === 0) return <h2 className="expenses-list__fallback">No Content</h2>

  return (
    <ul className="expenses-list">
        {props.items.map((elm)=>{           
          return <ExpenseItem key={elm.id} props={elm}/>
        })}
    </ul>
  )
}

export default ExpenseList