import React, { useState } from 'react'
import './Expense.css';
import Card from '../ui/Card';
import ExpensesFilter from './ExpenseFilter';
import ExpenseList from './ExpenseList';
import ExpenseCart from './ExpenseCart';

const Expense = (props) => {
  const [filterYear, setFilterYear] = useState('ALL');
  const filterExpenseHandler = (elm)=>{
    setFilterYear(elm)
  }
  const filterYearHandler = (filterYear === 'ALL') ? props.items: props.items.filter(expense => expense.date.getFullYear().toString()===filterYear);
  
  return (
    <div>
      <li>
        <Card className='expenses'>
        <ExpenseCart expenses={filterYearHandler}/>
        <ExpensesFilter selected={filterYear} onFilterExpense={filterExpenseHandler}/>
        {/* {filterYearHandler.length === 0 && <p>No Content</p> } 
        {filterYearHandler.length > 0 && filterYearHandler.map((elm)=>{           
          return <ExpenseItem key={elm.id} props={elm}/>
        })} */}
        <ExpenseList items={filterYearHandler} />
        {/* {expenseContent} */}
        </Card>
      </li>
    </div>
  )
}

export default Expense